package com.HashMap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class TestStu {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Student s1 = new Student("三粗", '男');
		Student s2 = new Student("四傻", '男');
		Student s3 = new Student("五大", '男');
		Student s4 = new Student("拉妹纸", '女');
		Student s5 = new Student("大美女", '女');
		Map<String, Student> map = new HashMap<String, Student>();
		map.put("Tom", s1);
		map.put("Jack", s2);
		map.put("Davie", s3);
		map.put("Lili", s4);
		map.put("Beat", s5);
		Set<String> set2 = map.keySet(); // 先取出key（索引）
		Iterator<String> it = set2.iterator(); // 定义一个Iterator迭代器
		System.out.println("学员姓名\t性别");
		while (it.hasNext()) {
			String str = it.next();
			Student stuValue = map.get(str);			
			System.out.println(stuValue.getName() + "\t"
					+ stuValue.getSex());
		}
		System.out.print("请输入你要查看学员的英文名：");
		String keyName = input.next();
		Student stu = map.get(keyName);
		// 寻找key的位置  判断索引是否存在 
		if (map.containsKey(keyName)) {
			System.out.println(keyName+"对应的学员姓名是:" + stu.getName() + "\t性别:"
					+ stu.getSex());
		} else {
			System.out.println("对不起！不存在该学员！");
		}
	}
}
