package com.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class WeekTest {
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();//
		cal.set(2015, 3, 6);
		int w=cal.get(Calendar.WEEK_OF_YEAR);
		System.out.println(sdf.format(cal.getTime())+"是这一年的第"+w+"个星期");
		 
		
		
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.MONTH, 4-1);//Calendar.MONTH的月份是从0开始的
		cal.set(Calendar.DATE, 6);
		int week = cal.get(Calendar.DAY_OF_WEEK);//DAY_OF_WEEK取星期几，星期是从星期天开始
		System.out.print(sdf.format(cal.getTime()) + "是 ");
		switch (week) {
		case 1:
			System.out.println("星期日");
			break;
		case 2:
			System.out.println("星期一");
			break;
		case 3:
			System.out.println("星期二");
			break;
		case 4:
			System.out.println("星期三");
			break;
		case 5:
			System.out.println("星期四");
			break;
		case 6:
			System.out.println("星期五");
			break;
		case 7:
			System.out.println("星期六");
			break;
		}		
	}
}
