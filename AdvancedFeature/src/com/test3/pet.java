package com.test3;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class pet {
	public static void main(String[] args) throws IOException {
		//FileReader fr = new FileReader("d:\\pet.txt");
		FileInputStream fis=new FileInputStream("d:\\pet.txt");
		//使用InputStreamReader并设置编码格式
		InputStreamReader fr=new InputStreamReader(fis,"GBK"); 
		BufferedReader bfr = new BufferedReader(fr);
		String line = null;
		String news="";
		System.out.print("替换前：");
		while ((line = bfr.readLine()) != null) {
			System.out.print(line);
			news=line;
		}
		//System.out.print(line);
		//line.replace(oldChar, newChar)
		news = news.toString().replace("{name}", "欧欧").toString()
				.replace("{type}", "阿拉斯加").toString().replace("{master}", "李伟");
		System.out.print("\n替换后：" + news);

		fr.close();
		bfr.close();
	}
}
