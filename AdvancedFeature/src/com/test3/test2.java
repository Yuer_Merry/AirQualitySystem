package com.test3;

import java.io.File;
import java.io.IOException;

public class test2 {
	public static void main(String[] args) throws IOException {
		File file = new File("D:\\ta.txt");
		System.out.println(file.exists());// 判断文件是否存在		
		file.createNewFile();// 创建文件，不是文件夹     file.mkdir();创建文件夹
		/*if(file.exists()){
			file.delete();
		}*/
		
		System.out.println(file.exists());// 再次判断是否存在
		System.out.println(file.getName());// 获取文件或目录的名字
		System.out.println(file.getAbsolutePath());// 获取文件的磁盘的路径绝对路径
		System.out.println(file.getPath());// 获取文件的相对路径
		System.out.println(file.getParent());// 获取文件的（父）上级文件路径
		System.out.println(file.canRead());// 文件是否可读
		System.out.println(file.canWrite());// 文件是否可写
		System.out.println("长度："+file.length());// 文件的长度
		System.out.println(file.lastModified());// 文件最后一次修改的时间
		System.out.println(file.isDirectory());// 判断文件是否是一个目录
		System.out.println(file.isHidden());// 文件是否隐藏
		System.out.println(file.isFile());// 判断文件是否存在		
	}
}
