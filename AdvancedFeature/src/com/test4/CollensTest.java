package com.test4;

import java.util.*;

public class CollensTest {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(10);
		list.add(5);
		list.add(3);
		
		System.out.println("排序前的数据");
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
		}
		
		Collections.sort(list);//排序的方法
		
		System.out.println("\n\n排序后的数据");
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
		}
		
		//求最大值
		int max = Collections.max(list);
		System.out.println("\n\nlist的最大值是:" + max);
		
		//求最小值
		int min = Collections.min(list);
		System.out.println("\n\nlist的最小值是:" + min);
		
		//根据数据查找对应的索引
		int search = Collections.binarySearch(list, 5);
		System.out.println("\n\nlist的数字5的索引是：" + search);
		
	}
}
