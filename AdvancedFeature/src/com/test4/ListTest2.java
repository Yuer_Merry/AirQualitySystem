package com.test4;

import java.util.ArrayList;
import java.util.List;

public class ListTest2 {
	public List<Course> list;
	
	/* 集合----Object
	 * 泛型集合 ---- 指定数据类型
	 */
	public ListTest2() {
		list = new ArrayList<Course>();
	}
	
	public void addCourse(){
		Course c1 = new Course(1,"JAVA高级特性");
		list.add(c1);
		System.out.println(list.get(0).getCourseName());
	}
	
	public static void main(String[] args) {
		ListTest2 lt = new ListTest2();
		lt.addCourse();
	}
}
