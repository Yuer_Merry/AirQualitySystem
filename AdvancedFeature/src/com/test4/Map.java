package com.test4;

import java.util.*;

public class Map {
	public static void main(String[] args) {
		HashMap<String, String> nation=new HashMap<String, String>();
		//put(K key, V value)
		nation.put("CN","中华人民共和国");		
		nation.put("FR","法兰西");
		nation.put("US","美利坚合众国");
		nation.put("RU","俄罗斯联邦");
		String n=nation.get("CN");
		System.out.println("CN对应的国家是"+n);
		System.out.println("Map中共有"+nation.size()+"组数据");
		System.out.println("Map中共有含有FR的Key吗？"+nation.containsKey("FR"));
		nation.remove("FR");
		System.out.println("Map中共有含有FR的Key吗？"+nation.containsKey("FR"));
		System.out.println(nation.keySet());
		System.out.println(nation.values());
		System.out.println("已清空Map所有数据");
	}
}
