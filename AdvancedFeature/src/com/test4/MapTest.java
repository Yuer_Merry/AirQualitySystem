package com.test4;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapTest {
	public static void main(String[] args) {
		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1,"java基础语法");
		map.put(2, map.get(1));
		map.put(6, "JAVA高级特性");
		map.put(8, map.get(1));
		map.put(20, map.get(1));
		//map.remove(1);
		
		
		
		//返回所有值(value)的集合
		Collection<String> c = map.values();
		Iterator<String> it2 = c.iterator();
		while(it2.hasNext()){
			System.out.print(it2.next() + ",");
		}
		System.out.println();
		
		
		//返回所有键(key)的集合
		Set<Integer> set = map.keySet();
		Iterator<Integer> it = set.iterator();
		while(it.hasNext()){
			System.out.print(it.next() + ",");
		}
		
		
		System.out.println();
		System.out.println(map.get(1));
		System.out.println(map.get(2));
		
		if(map.containsValue("JAVA高级特性")){
			System.out.println("存在");
		}else{
			System.out.println(map.get(6));
		}
		
	}
}
