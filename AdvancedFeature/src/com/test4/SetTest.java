package com.test4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SetTest {
	public static void main(String[] args) {
		Set<String> set = new HashSet<String>();
		set.add("alex");
		set.add("luck");
		set.add("lisa");
		set.add("judy");
		System.out.println(set.size());
		
		Iterator<String> it = set.iterator();
		//it.hasNext() 按链表查询，只要有数据返回true,否则返回false
		while(it.hasNext()){
			System.out.print(it.next() + " ");
		}
		
		
		//泛型只能写包装类的数据类型，不能使用基础数据类型
		List<Integer> list = new ArrayList<Integer>();
		
		Integer[] itg = {1,2,3,7,5,6};
		
		//将一个数组直接全部add到集合中
		list.addAll(Arrays.asList(itg));
		
		System.out.println("\nlist集合内容总数：" + list.size());
		
		Iterator<Integer> it2 = list.iterator();
		while(it2.hasNext()){
			System.out.print(it2.next() + ",");
		}
		
//		list.add(1);
//		list.add(2);
//		list.add(3);
//		list.add(4);
//		list.add(5);
		//key:value
		
		//key    项目经理
		//key    保洁
		//key    前台
		//key    电工
		
		
		//value  人   内容可以重复，key是唯一的
		
	}
}

