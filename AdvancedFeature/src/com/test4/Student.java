package com.test4;

import java.util.HashSet;
import java.util.Set;

public class Student {
	private int stuID;
	private String stuName;
	public Set Courses;
	public int getStuID() {
		return stuID;
	}
	public void setStuID(int stuID) {
		this.stuID = stuID;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public Student(){
		
	}
	public Student(int stuID, String stuName) {
		super();
		this.stuID = stuID;
		this.stuName = stuName;
		this.Courses = new HashSet();
	}
}
