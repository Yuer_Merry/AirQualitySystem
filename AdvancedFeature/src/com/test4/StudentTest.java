package com.test4;

import java.util.ArrayList;
import java.util.List;

public class StudentTest {
	
//	public List list;
//	
//	public StudentTest(){
//		list = new ArrayList();
//	}
//	
//	//添加课程的方法
//	//list集合存储course对象
//	public void addCourse(){
//		Course c1 = new Course(1, "JAVA基础语法");
//		this.list.add(c1);
//	}
//	
//	//使用foreach循环遍历list集合里面的数据
//	public void showCourse(){
//		for (Object obj : list) {
//			Course c = (Course)obj;
//			System.out.println("课程的ID：" + c.getCourseID() + "，课程的名称：" + c.getCourseName());
//		}
//	}
//	
//	public static void main(String[] args) {
//		StudentTest st = new StudentTest();
//		
//		st.addCourse();
//		
//		st.showCourse();
//		
//	}
	
	//List集合，任意的内容存储进去都会变成Object类型
	public static void main(String[] args) {
		List list = new ArrayList();
		
		Course c1 = new Course(1, "JAVAOOP");
		
		list.add(c1);
		
//		Object obj = c1;
//		Course c3 = (Course)obj;
		
		//list.get(0);
		Course c2 = (Course)list.get(0);
		
		System.out.println(c1.getCourseName());
		System.out.println(c2.getCourseName());
		//System.out.println(c3.toString());
	}
}
