package com.test4;

import java.util.*;


public class test {
	public static void main(String[] args) {
		Dog d1 = new Dog("欧欧", "哈士奇");
		Dog d2 = new Dog("亚亚", "阿拉斯加");
		Dog d3 = new Dog("菲菲", "阿拉斯加");
		Dog d4 = new Dog("美美", "哈士奇");
		List<Dog> dogs = new ArrayList<Dog>();		
		dogs.add(d1);
		dogs.add(d2);
		dogs.add(d4);
		dogs.add(3, d3);//指定插入位置
		System.out.println("共计有：" + dogs.size() + "条狗狗");
		System.out.println("分别是：");
		for (int i = 0; i < dogs.size(); i++) {
			Dog dog = (Dog) dogs.get(i);
			System.out.println(dog.getName() + "\t" + dog.getType());
		}
		System.out.println("删除前有" + dogs.size() + "条狗狗\n");
		dogs.remove(d1);
		dogs.remove(d3);
		System.out.println("删除后有" + dogs.size() + "条狗狗");
		for (int i = 0; i < dogs.size(); i++) {
			Dog dog = (Dog) dogs.get(i);
			System.out.println(dog.getName() + "\t" + dog.getType());
		}
		if (dogs.contains(d2)) {
			System.out.println("集合中包含美美的的信息");
		} else {
			System.out.println("集合中不包含美美的的信息\n\n");
		}
		LinkedList<Dog> d = new LinkedList<Dog>();
		d.addFirst(d1);
		Dog dogFirst = (Dog) d.getFirst();
		System.out.println("\n\n第一条狗狗的昵称是" + dogFirst.getName());
		d.addLast(d4);
		Dog dogLast = (Dog) d.getLast();
		System.out.println("最后条狗狗的昵称是" + dogLast.getName());
		d.removeFirst();
		d.removeLast();
		System.out.println("删除后有" + dogs.size() + "条狗狗\n分别是：");
		for (int i = 0; i < dogs.size(); i++) {
			Dog dog = (Dog) dogs.get(i);
			System.out.println(dog.getName() + "\t" + dog.getType());
		}
	}
}
