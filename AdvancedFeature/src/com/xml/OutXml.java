package com.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OutXml {
	public DocumentBuilder getDocumentBuilder() throws Exception {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();

		return db;
	}

	public void show() throws Exception {
		Document document = this.getDocumentBuilder().newDocument();
		document.setXmlStandalone(true);
		Element animal = document.createElement("animal");
		Element dog = document.createElement("dog");
		Element name = document.createElement("name");
		Element type = document.createElement("type");
		Element age = document.createElement("age");
		dog.appendChild(age);
		dog.appendChild(type);
		dog.appendChild(name);//dog包含name
		animal.appendChild(dog);//animal包含dog
		document.appendChild(animal);//document必须在后面
		
		//给pet元素赋属性值animal.setAttribute("id", "pet1");
		
		name.setTextContent("阿拉阿拉");//内容赋值
		type.setTextContent("阿拉斯加");
		age.setTextContent("1");
		
		TransformerFactory tff = TransformerFactory.newInstance();
		Transformer tf = tff.newTransformer();
		// 创建XML文档有换行符
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		// 创建XML文档，并且将Document里面的XML元素创建
		tf.transform(new DOMSource(document), new StreamResult(new File(
				"demo\\Animals.xml")));
	}

	public static void main(String[] args) throws Exception {
		OutXml ox = new OutXml();
		ox.show();
	}
}
