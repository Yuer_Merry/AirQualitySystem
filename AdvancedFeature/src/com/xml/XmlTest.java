package com.xml;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlTest {
	public DocumentBuilder getDocumentBuilder() throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db;
	}

	public void show() throws Exception {
		Document document = getDocumentBuilder().parse("demo\\score.xml");
		NodeList people = document.getElementsByTagName("people");
		for (int i = 0; i < people.getLength(); i++) {
			Node node = people.item(i);
			System.out.println();
			NodeList childlist = node.getChildNodes();
			for (int j = 0; j < childlist.getLength(); j++) {
				// 移除子元素的换行符，#text
				if (childlist.item(j).getNodeType() == Node.ELEMENT_NODE) {
					System.out.println(childlist.item(j).getTextContent());
					//childlist.item(j).getNodeName()显示标签
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		XmlTest xt = new XmlTest();
		xt.show();
	}
}
