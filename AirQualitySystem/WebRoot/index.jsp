<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC >
<html>
<head>
<base href="<%=basePath%>">

<title>电子文档</title>

<link rel="stylesheet" href="layerui/css/layui.css" type="text/css"></link>
<style type="text/css">
.main_div {
	width: 1000px;
	margin: auto;
}
</style>
</head>

<body>
	<div class="main_div">
		<div>
			<label>按区域查询：</label>
			<form action="airQualityIndexAction" method="post">

				<select name="districtId">
					<option value="">不限</option>
					<c:forEach items="${districtList }" var="district">
						<c:if test="${district.id eq districtId }">
							<option selected="selected" value="${district.id }">${district.name }</option>
						</c:if>
						<c:if test="${district.id ne districtId }">
							<option value="${district.id }">${district.name }</option>
						</c:if>

					</c:forEach>
				</select> <input type="submit" value="查询" />
			</form>


		</div>
		<table>
			<colgroup>
				<col width="150">
				<col width="200">
				<col>
			</colgroup>
			<thead>
				<!--表格头部-->
				<tr style="background-color: #CCE8CF" align="center">
					<th colspan="8" align="center"><br />
						<h2>电子文档列表</h2>
					</th>
				</tr>
				<tr style="background-color: #CCE8CF">
					<th>序号</th>
					<th>区域</th>
					<th>检测时间</th>
					<th>PM10数据</th>
					<th>PM2.5数据</th>
					<th>监测站</th>
				</tr>
			</thead>
			<tbody>
				<!--表格内部-->
				<c:forEach items="${list}" var="info" varStatus="a">
					<c:if test="${a.index % 2 eq 0 }">
						<tr style="background-color: #E5B8D9">
							<td>${info.id }</td>
							<td>${info.name }</td>
							<td>${info.mt }</td>							
							<td>${info.pm10 }</td>
							<td>${info.pm25 }</td>
							<td>${info.ms }</td>
							<td>${info.lm }</td>
							<td><a href="airQualityIndexAction?action=update&id=${info.id } ">修改 </a> <a href="airQualityIndexAction?action=delete&id=${info.id }">删除</a></td>

						</tr>
					</c:if>
					<c:if test="${a.index % 2 ne 0 }">
						<tr style="background-color: #F2CFE7">
							<td>${info.id }</td>
							<td>${info.name }</td>
							<td>${info.mt }</td>							
							<td>${info.pm10 }</td>
							<td>${info.pm25 }</td>
							<td>${info.ms }</td>
							<td>${info.lm }</td>
							<td><a href="airQualityIndexAction?action=update&id=${info.id }">修改 </a> <a href="airQualityIndexAction?action=delete&id=${info.id }">删除</a></td>
						</tr>
					</c:if>
				</c:forEach>
				<tr style="background-color: #E5B8D9">
					<td colspan="8"><a href="categoryAction" class="layui-btn">新增数据</a>&nbsp;&nbsp;&nbsp;
						<form action="airQualityIndexAction?action=findAll" method="post">
							共${totalTable }条记录&nbsp;&nbsp; 每页<input type="text" name="pageSize" style="width: 20px" value="${pageSize }" /> 条数据&nbsp;&nbsp;&nbsp; 第 ${indexPage } /共 ${totalPage}页&nbsp;&nbsp;&nbsp; <a href="airQualityIndexAction?indexPage=1&pageSize=${pageSize }">第一页</a>&nbsp;&nbsp;&nbsp;

							<c:if test="${indexPage == 0}">
								<a href="airQualityIndexAction?indexPage=${indexPage - 1 }&pageSize=${pageSize }">上一页</a>&nbsp;&nbsp;&nbsp;
					</c:if>
							<c:if test="${indexPage >1}">
								<a href="airQualityIndexAction?indexPage=${indexPage - 1 }&pageSize=${pageSize }">上一页</a>&nbsp;&nbsp;&nbsp;
					</c:if>

							<c:if test="${indexPage <= totalPage-1 }">
								<a href="airQualityIndexAction?indexPage=${indexPage + 1 }&pageSize=${pageSize }">下一页</a>&nbsp;&nbsp;&nbsp;
						</c:if>

							<a href="airQualityIndexAction?indexPage=${totalPage }&pageSize=${pageSize }">尾页</a>&nbsp;&nbsp;&nbsp; 跳转到<input type="text" name="indexPage" value="${indexPage }" style="width: 20px" />页 <input type="submit" value="go" />
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="layerui/layui.js"></script>
</body>
</html>
