package com.AirQualitySystem.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface AirQualityIndexMapper {
	
	public List<Map<String, Object>> findPageAll(@Param("districtId") Integer districtId, @Param("indexPage") Integer indexPage, @Param("pageSize") Integer pageSize);

	public int findTotalpage(Integer districtId);

} 
