package com.AirQualitySystem.dao;

import java.util.List;
import java.util.Map;

public interface DistrictMapper {
	public List<Map<String, Object>> findAll();
}
