package com.AirQualitySystem.server;

import java.util.List;
import java.util.Map;



public interface AirQualityIndexServer {
	public List<Map<String, Object>> findPageAll(Integer districtId, Integer indexPage,Integer pageSize);

	public int findTotalPage(Integer districtId, Integer pageSize);
}
