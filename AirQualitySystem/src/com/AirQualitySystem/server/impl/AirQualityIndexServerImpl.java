package com.AirQualitySystem.server.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.AirQualitySystem.dao.AirQualityIndexMapper;
import com.AirQualitySystem.server.AirQualityIndexServer;
import com.AirQualitySystem.util.BaseDao;

/*
 * ҵ���߼�������
 *
 */
public class AirQualityIndexServerImpl implements AirQualityIndexServer {

	public List<Map<String, Object>> findPageAll(Integer districtId, Integer indexPage, Integer pageSize) {
		BaseDao dao = new BaseDao();
		SqlSession session = dao.getSession();
		AirQualityIndexMapper aiDao = session.getMapper(AirQualityIndexMapper.class);

		indexPage = (indexPage - 1) * pageSize;

		List<Map<String, Object>> list = aiDao.findPageAll(districtId, indexPage, pageSize);
		dao.close(session);

		return list;
	}

	public int findTotalPage(Integer districtId, Integer pageSize) {
		BaseDao dao = new BaseDao();
		SqlSession session = dao.getSession();
		AirQualityIndexMapper aiDao = session.getMapper(AirQualityIndexMapper.class);
		int result = aiDao.findTotalpage(districtId);

		dao.close(session);
		return result % pageSize == 0 ? result / pageSize : result / pageSize + 1;
	}

}
