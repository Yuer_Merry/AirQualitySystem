package com.AirQualitySystem.server.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.AirQualitySystem.dao.DistrictMapper;
import com.AirQualitySystem.server.DistrictServer;
import com.AirQualitySystem.util.BaseDao;

public class DistrictServerImpl implements DistrictServer {

	public List<Map<String, Object>> findAll() {
		BaseDao dao = new BaseDao();
		SqlSession session = dao.getSession();
		DistrictMapper dmDao = session.getMapper(DistrictMapper.class);
		List<Map<String, Object>> list = dmDao.findAll();

		dao.close(session);

		return list;
	}

}
