package com.AirQualitySystem.util;

import java.io.IOException;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class BaseDao {
	public SqlSession getSession() {
		SqlSession session = null;
		try {
			SqlSessionFactory sf = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("MyBatis.xml"));
			session = sf.openSession();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session;
	}

	public void close(SqlSession session) {
		if (session != null) {
			session.commit();
			session.close();
		}

	}
}
