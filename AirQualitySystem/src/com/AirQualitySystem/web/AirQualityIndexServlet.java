package com.AirQualitySystem.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.AirQualitySystem.server.AirQualityIndexServer;
import com.AirQualitySystem.server.DistrictServer;
import com.AirQualitySystem.server.impl.AirQualityIndexServerImpl;
import com.AirQualitySystem.server.impl.DistrictServerImpl;


public class AirQualityIndexServlet extends HttpServlet {

	private static final long serialVersionUID = 6940052082735894943L;
	AirQualityIndexServer aqi = new AirQualityIndexServerImpl();
	DistrictServer ds = new DistrictServerImpl();

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
 
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action == null || "findPageAll".equals(action)) {
			// 条件分页查询
			this.findPageAll(request, response);
		}
		
	}

	private void findPageAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String str = request.getParameter("indexPage");
		if (str == null) {
			str = "1";

		}
		Integer indexPage = Integer.valueOf(str);
		Integer pageSize = 5;

		Integer districtId = null;
		str = request.getParameter("districtId");
		if (str != null && str.length() > 0) {
			districtId = Integer.valueOf(str);
		}
		// 调用业务逻辑处理
		List<Map<String, Object>> list = aqi.findPageAll(districtId, indexPage, pageSize);
		request.setAttribute("list", list);
		request.setAttribute("indexPage", indexPage);
		request.setAttribute("totalPage", aqi.findTotalPage(districtId, pageSize));

		request.setAttribute("districtList", ds.findAll());

		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
